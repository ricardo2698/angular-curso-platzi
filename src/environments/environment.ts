// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url_api: 'https://platzi-store.herokuapp.com',
  firebase: {
    apiKey: 'AIzaSyAs-tUFBt2OgOtX9z5npoI3RBagyWYtbEw',
    authDomain: 'platzi-store-e70eb.firebaseapp.com',
    projectId: 'platzi-store-e70eb',
    storageBucket: 'platzi-store-e70eb.appspot.com',
    messagingSenderId: '767134062431',
    appId: '1:767134062431:web:047226265d9a42aa1d08ea',
    measurementId: 'G-FZN8TLEJP8'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
