import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, } from '@angular/forms';

import { ProductsService } from '../../../../core/services/products/products.service';
import { MyValidators } from '../../../../utils/validators';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { Product } from 'src/app/core/models/Product';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit {

  form: FormGroup;
  id: string;

  constructor(
    private formBuilder: FormBuilder,
    private productsService: ProductsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private productService: ProductsService
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      (params:Params) =>{
        this.id  = params.id;
        this.productService.getProduct(this.id).subscribe(
            product => {
              this.form.patchValue(product);
            }
        );
      } 
    )
  };

  private buildForm(){
    this.form = this.formBuilder.group({
      title: ['', [Validators.required]],
      price: [, [Validators.required, MyValidators.isPriceValid],],
      image: [''],
      description: ['', [Validators.required]]
      
    })
  };

  save( event: Event ){
    event.preventDefault();
    console.log(this.form.value);
    if( this.form.valid ){
      const product = this.form.value;
      this.productsService.editProduct(this.id, product).subscribe(
        newProduct => {
          console.log(newProduct);
          this.router.navigate(['./admin/products'])
        }
      )
    }
  }

  get priceField (){
    return this.form.get('price');
  }

}
