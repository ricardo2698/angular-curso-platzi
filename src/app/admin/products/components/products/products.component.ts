import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/core/models/product';


import { ProductsService } from '../../../../core/services/products/products.service';


@Component({
  selector: 'app-products-list',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products: Product[] = [];
  idn: number;
  displayedColumns: string[] = ['id', 'title', 'price','accions']; 

  constructor(
    private productsService: ProductsService
  ) { }

  ngOnInit(): void {
    this.fecthProducts();
  }

  fecthProducts(){
    this.productsService.getAllProduct().subscribe(
      products => {
        this.products = products;
        console.log(this.products);
      }
    )
  }

  eliminarProduct(id:string ){
    this.productsService.deleteProduct(id).subscribe(
      delet=> {
        console.log(delet);
        
        if(delet){
          this.products = this.products.filter(product => product.id !== id);
        }
        else{
          console.log(' No se pudo eliminar el elemento');
        }
        
      }
    )
  }


}
