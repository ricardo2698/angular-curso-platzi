import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';


import { ProductsService } from '../../../../core/services/products/products.service';
import { AngularFireStorage } from '@angular/fire/storage';

import { MyValidators } from '../../../../utils/validators';

import { Router } from '@angular/router';
import { Observable } from 'rxjs';




@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.scss']
})
export class ProductCreateComponent implements OnInit {

  form: FormGroup;
  image$: Observable<any>;

  constructor(
    private formBuilder: FormBuilder,
    private productsService: ProductsService,
    private router: Router,
    private storage: AngularFireStorage
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
  }

  uploadFile( event){
    const file = event.target.files[0];
    const dir = 'images';
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);

    task.snapshotChanges()
    .pipe(
      finalize( ()=> {
        this.image$ = fileRef.getDownloadURL();
        this.image$.subscribe(url => {
          console.log(url); 
          this.form.get('image').setValue(url);
        } )
      } )
    )
    .subscribe();
  }

  private buildForm(){
    this.form = this.formBuilder.group({
      id: ['', [Validators.required]],
      title: ['', [Validators.required]],
      price: [, [Validators.required, MyValidators.isPriceValid],],
      image: [''],
      description: ['', [Validators.required]]
      
    })
  };

  save( event: Event ){
    event.preventDefault();
    console.log(this.form.value);
    if( this.form.valid ){
      const product = this.form.value;
      this.productsService.createProduct(product).subscribe(
        newProduct => {
          console.log(newProduct);
          this.router.navigate(['./admin/products'])
        }
      )
    }
  }

  get priceField (){
    return this.form.get('price');
  }


}
