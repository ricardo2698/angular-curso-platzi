import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

// routing (rutas)
import { AdminRoutingModule } from './admin-routing.module';

// Modulo Material Angular
import { MaterialModule } from '../material/material.module';

// Components
import { NavComponent } from './components/nav/nav.component';
import { InventarioComponent } from './components/inventario/inventario.component';
import { BasicFormComponent } from './components/basic-form/basic-form.component';




@NgModule({
  declarations: [
    NavComponent, 
    InventarioComponent, BasicFormComponent,
    
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
  ]
})
export class AdminModule { }
