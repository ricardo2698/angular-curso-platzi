import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { ContactRoutingModule } from './contact-routing.module';
import { FormsModule } from '@angular/forms';


import { ContactComponent } from './components/contact/contact.component';

@NgModule({
    declarations: [
        ContactComponent
    ],
    imports: [ 
        CommonModule,
        SharedModule,
        ContactRoutingModule,
        FormsModule
    ],
    exports: [],
    providers: [],
})
export class ContactModule {}