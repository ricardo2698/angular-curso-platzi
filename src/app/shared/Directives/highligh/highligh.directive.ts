import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[appHighligh]'
})
export class HighlighDirective  implements OnInit {

  constructor(
    private element: ElementRef
  ) 
  {
    
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.element.nativeElement.style.backgroundColor = 'red';
  }

}
