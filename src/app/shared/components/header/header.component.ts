import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


import { CartService } from '../../../core/services/cart.service';

import { Product } from '../../../core/models/product';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  total$ :Observable<number>; // nuevo tipo de observable

  constructor(
    private cartService: CartService 
  ) { }

  ngOnInit(): void {

    this.total$ = this.cartService.cart$.
    pipe(map(product => product.length));
    
  }

}
