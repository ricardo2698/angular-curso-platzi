import { Component, OnInit } from '@angular/core';
import { Product } from '../../../core/models/product';
import { ProductsService } from '../../../core/services/products/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor( private productService: ProductsService) { }

  products: Product[]= [];

  ngOnInit(){
    this.fecthProducts();
  }

  clickProduct(id:number){
    console.log('componente padre');
    console.log(id);
  }

  fecthProducts(){
    this.productService.getAllProduct().subscribe(
      data => {
        this.products = data;
      }
    );
    console.log(this.products);
  }

}
