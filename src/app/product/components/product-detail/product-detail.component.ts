import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Product } from '../../../core/models/Product';
import { ProductsService } from '../../../core/services/products/products.service';


@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  product: Product;
  constructor(
    private route: ActivatedRoute,
    private productService: ProductsService
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params.id;
    console.log(id);

    this.fecthProduct(id);
  }

  fecthProduct(id:string){
    

/*     this.route.params.subscribe(
      (params: Params) => { 
        console.log(params);
      }
    ); */

    this.productService.getProduct(id).subscribe(
      data=> {
        console.log(data);
        this.product = data;
      }
    );
  }

  createProduct(){
    const newProduct: Product = {
      id: '10',
      image: 'assets/images/banner-3.jpg',
      title: 'Nuevo desde Angular',
      price: 10000,
      description: 'Ricardo',
    }
    this.productService.createProduct(newProduct).subscribe(
      data=> {
        this.product = data;
        console.log(this.product);
      }
    );
  };

  updateProduct(){
    const updateProduct: Partial<Product> = {
      price: 100000,
      description: 'Editado por ricardo',
    };

    this.productService.editProduct('6',updateProduct).subscribe(
      data=> {
        this.product = data;
        console.log(this.product);
      }
    );

  };

  deleteProduct( id:string ){
    this.productService.deleteProduct('7').subscribe(
      data=> {
        this.product = data;
        console.log(this.product);
      }
    );
  }
  

}
