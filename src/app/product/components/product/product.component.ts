import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Product } from 'src/app/core/models/Product';

import { CartService } from '../../../core/services/cart.service';



@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnChanges {

  @Input() product: Product;

  today =  new Date(); 

  power = 10;

  @Output() productClicked = new EventEmitter<any>();

  constructor(
    private cartService: CartService
  ) { 
  }

  ngOnChanges(changes:SimpleChanges) {
    /* console.log('ngOnChanges');
    console.log(changes); */
  }

  ngOnInit(): void {
    /* console.log('ngOnInit'); */
  }

  ngDoCheck(): void {
    //Called every time that the input properties of a component or a directive are checked. Use it to extend change detection by performing a custom check.
    //Add 'implements DoCheck' to the class.
    
  }

  agregarCarrito(product: Product){
    console.log('añadir al carrito, componente hijo');
    console.log(product);
    /* this.productClicked.emit(product.id); */
    this.cartService.addCart(this.product);

  }

}
