import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit, AfterViewInit } from '@angular/core';
  // import Swiper bundle with all modules installed
  import SwiperCore, {
    Navigation,
    Pagination,
    Scrollbar,
    A11y,
    Swiper,
  } from 'swiper/core';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {

  public mySwiper: Swiper;

  constructor() { }

  ngAfterViewInit(){
    this.mySwiper = new Swiper('.swiper-container');
  }

  ngOnInit(): void {
  }

}
