import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { HomeComponent } from '../home/Components/home/home.component';
import { BannerComponent } from '../home/Components/banner/banner.component';
import { HomeRoutingModule } from '../home/home-routing.module';

import { SharedModule } from '../shared/shared.module';




@NgModule({
    declarations: [
        HomeComponent,
        BannerComponent
    ],
    imports: [
        CommonModule,
        HomeRoutingModule ,
        SharedModule
    ],
    exports: [ 
    ],
    providers: [],
})
export class HomeModule {}