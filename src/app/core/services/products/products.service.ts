import { Injectable } from '@angular/core';
import { Product } from '../../models/Product';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../../environments/environment';



@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  /* private baseUrl: string = 'https://platzi-store.herokuapp.com/products'; */

  private baseUrl: string = environment.url_api;

  constructor(
    private http: HttpClient
  ) { }


  products: Product[];

  /**
  * Lista de productos de nuestra API
  */
  getAllProduct(): Observable<Product[]>{
    return this.http.get<Product[]>(`${this.baseUrl}/products`);
  }

  /**
 * Busca un producto según su id
 * @param id Identificador del producto a buscar
 */
  getProduct( id:string ){
    /* return this.products.find(item => item.id === id); */
    return this.http.get<Product>(`${this.baseUrl}/products/${id}`);
  }

  createProduct( product:Product ){
    return this.http.post<Product>(`${this.baseUrl}/products`,product);
  }

  editProduct( id:string, changes: Partial<Product>){
    return this.http.put<Product>(`${this.baseUrl}/products/${id}`,changes);
  }

  deleteProduct( id:string){
    return this.http.delete<Product>(`${this.baseUrl}/products/${id}`);
  }


}
