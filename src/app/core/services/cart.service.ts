import { Injectable } from '@angular/core';
import { Product } from '../models/product';

import { BehaviorSubject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class CartService {

  private product: Product[] = [];
  private cart = new BehaviorSubject<Product[]>([]);

  cart$ = this.cart.asObservable();

  constructor() { }

  addCart( prodcut: Product ){
    this.product = [...this.product, prodcut];
    this.cart.next(this.product);
  }
}
