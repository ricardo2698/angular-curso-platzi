import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { DemoRoutingModule } from './demo-roiting.module';
import { SharedModule } from '../shared/shared.module';

import { DemoComponent } from './components/demo/demo.component';

@NgModule({
    declarations: [
        DemoComponent
    ],
    imports: [ 
        CommonModule,
        SharedModule,
        DemoRoutingModule,
        FormsModule
    ],
    exports: [],
    providers: [],
})
export class DemoModule {}