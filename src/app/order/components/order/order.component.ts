import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Product } from 'src/app/core/models/product';

import { CartService } from '../../../core/services/cart.service';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  poducts$: Observable<Product[]>;

  constructor(
    private cartService: CartService
  ) { 
    this.poducts$ = this.cartService.cart$
  }

  ngOnInit(): void {
    console.log(this.poducts$);
  }

}
